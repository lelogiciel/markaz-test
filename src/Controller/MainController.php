<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;



class MainController extends AbstractController{

    #[Route('/', name:'page-accueil')]
    public function mainPage(){

        return $this->render('default/accueil.html.twig');
    }


    #[Route('/membres', name:'page-membres')]
    public function pageMembre(){

        return $this->render('default/membres.html.twig');
    }


    #[Route('/mission', name:'page-mission')]
    public function pageMission(){

        return $this->render('default/mission.html.twig');
    }


    #[Route('/contact', name:'page-contact')]
    public function pageContact(){

        return $this->render('default/contact.html.twig');
    }
}
